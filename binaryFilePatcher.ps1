# python3
# Article: http://penconsultants.com/subpage/blog/post/binary-file-patching-scripts
# This will patch the end of all files in the specified directory that match the hard coded filter to ensure each victim receives unique file hashes.
# Note, some files may not support this (they may break) if they do not have sufficient padding at the end and/or have any sort of file integrity check.

# powershell -exe bypass -file binaryFilePatcher.ps1 -patchNum 205 -folderPath c:\temp\build\exe.win32-3.4

param (
    [Parameter(Mandatory=$true)][int]$patchNum = $false,
    [Parameter(Mandatory=$true)][string]$folderPath = $false
)

write-host $folderPath
If (!(Test-Path $folderPath)) {
    write-host "path does not exists? Exiting"
    exit
}

Get-ChildItem $folderPath\* -Include *.pyd, *.exe |
Foreach-Object {
    Write-Host $_.FullName
    $bytes = [system.io.file]::ReadAllBytes($_.FullName)
    $bytes[$bytes.length-1] = [byte]$patchNum
    [system.io.file]::WriteAllBytes($_.FullName,$bytes)
}